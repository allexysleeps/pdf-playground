const { PDFDocument, StandardFonts, rgb, degrees } = require('pdf-lib')
const path = require('path')
const fs = require('fs')

const oldPDFData = fs.readFileSync(path.resolve(__dirname, '../resources/regular-form.pdf'))


const drawText = async (page, text, options) => {
  return page.drawText(text, options)
}

const updatePdf = async () => {
  const pdfDoc = await PDFDocument.load(oldPDFData)
  const helveticaFont = await pdfDoc.embedFont(StandardFonts.Helvetica)
  const pages = pdfDoc.getPages()
  
  let { width, height } = pages[0].getSize()

  await drawText(pages[0], 'My company name', {
    x: 160,
    y: height - 205,
    size: 12,
    color: rgb(1, 0, 0),
    font: helveticaFont
  })
  
  await drawText(pages[0], 'My trading name', {
    x: 145,
    y: height - 222,
    size: 12,
    color: rgb(1, 0, 0),
    font: helveticaFont
  })
  
  await drawText(pages[0], '+', {
    x: 190,
    y: height - 187,
    size: 12,
    color: rgb(1, 0, 0),
    font: helveticaFont
  })
  
  await drawText(pages[0], 'Company billing name', {
    x: 135,
    y: height - 272,
    size: 12,
    color: rgb(1, 0, 0),
    font: helveticaFont
  })
  
  await drawText(pages[0], 'Company street name', {
    x: 80,
    y: height - 290,
    size: 12,
    color: rgb(1, 0, 0),
    font: helveticaFont
  })
  
  await drawText(pages[0], 'Company town name', {
    x: 80,
    y: height - 306,
    size: 12,
    color: rgb(1, 0, 0),
    font: helveticaFont
  })
  
  await drawText(pages[0], 'Company country name', {
    x: 84,
    y: height - 324,
    size: 12,
    color: rgb(1, 0, 0),
    font: helveticaFont
  })
  
  const pdfBytes = await pdfDoc.save()
  fs.writeFileSync(path.resolve(__dirname, '../dist/draw-on-pdf.pdf'), pdfBytes)
}

updatePdf()