const express = require('express')
const path = require('path')
const cors = require('cors')

const app = express()

  app
    .use(cors())
    .get('/pdf', (req, res) => {
      res.sendFile(path.resolve(__dirname, '../resources/regular-form.pdf'), {
        headers: {
          'Content-type': 'application/pdf'
        }
      })
    })
    .listen(9000, () =>  console.log('Server is running on localhost:9000'))