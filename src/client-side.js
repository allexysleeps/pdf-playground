import { PDFDocument, StandardFonts, rgb, degrees } from 'pdf-lib'
import axios from 'axios'

const getPDFData = () => axios.get('http://localhost:9000/pdf', {
  responseType: 'arraybuffer'
})
  .then((res) => res.data)
  .then((data) => { console.log(data); return data })


const drawOnPdf = (oldPDFData) => PDFDocument.load(oldPDFData)
  .then(pdfDoc => {
    console.log('here')
    const pages = pdfDoc.getPages()
  
    let { width, height } = pages[0].getSize()
  
    pages[0].drawText( 'My company name', {
      x: 160,
      y: height - 205,
      size: 12,
      color: rgb(1, 0, 0),
    })
    
    return pdfDoc.save()
  })

const saveFile = (data) => {
  const file = new Blob([data], { type: 'application/pdf' })
  
  const linkNode = document.createElement('a')
  const url = URL.createObjectURL(file)
  linkNode.href = url
  linkNode.download = 'filename'
  document.body.appendChild(linkNode)
  linkNode.click()
  setTimeout(() => {
    document.body.removeChild(linkNode)
    window.URL.revokeObjectURL(url)
  }, 0)
}

getPDFData()
  .then(drawOnPdf)
  .then(saveFile)

