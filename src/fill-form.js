const path = require('path')
const pdfForm = require('pdfform.js')
const fs = require('fs')

const sourcePDF = fs.readFileSync(path.resolve(__dirname, '../resources/fillable-form.pdf'))
const distPDF = path.resolve(__dirname, '../dist/fillable-form-output.pdf')


const data = {
  company_name: ['Company name data'],
  trading_name: 'Trading name data',
  sole_trader: true
}

const output = pdfForm().transform(sourcePDF, data)

fs.writeFileSync(distPDF, output)



